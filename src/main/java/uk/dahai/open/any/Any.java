package uk.dahai.open.any;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.io.Serial;
import java.io.Serializable;
import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

@JsonDeserialize( converter = MapAnyConverter.class )
public final class Any implements Map<String, Object>, Serializable
{
    @Serial
    private static final long serialVersionUID = 1283027183833299811L;

    private final Map<String, Object> map;

    public Any()
    {
        this.map = new ConcurrentHashMap<>();
    }

    private Any( Map<String, Object> map )
    {
        this.map = map;
    }

    public static Any as( Map<String, ? extends Object> other )
    {
        return Any.bldr().as( other ).build();
    }

    public static Any of( String key, Object value )
    {
        return Any.bldr().with( key, value ).build();
    }

    public static Any of( String key1, Object value1, String key2, Object value2 )
    {
        return Any.bldr().with( key1, value1 ).with( key2, value2 ).build();
    }

    public Object get( Object key )
    {
        return this.map.get( key );
    }

    public <T> T get( String key, Class<T> clazz )
    {
        return clazz.cast( this.map.get( key ) );
    }

    public <T> T get( String key, T defaultValue )
    {
        Object value = this.map.get( key );
        return value == null ? defaultValue : (T) value;
    }

    public Integer getInteger( String key )
    {
        return (Integer) this.get( key );
    }

    public Long getLong( String key )
    {
        return (Long) this.get( key );
    }

    public BigDecimal getBigDecimal( String key )
    {
        return BigDecimal.valueOf( this.get( key, Number.class ).doubleValue() );
    }

    public Double getDouble( String key )
    {
        return (Double) this.get( key );
    }

    public Number getNumber( String key )
    {
        return (Number) this.get( key );
    }


    public String getString( String key )
    {
        return (String) this.get( key );
    }

    public Boolean getBoolean( String key )
    {
        return (Boolean) this.get( key );
    }

    public <T> List<T> getList( String key, Class<T> clazz )
    {
        return (List<T>) this.getOrDefault( key, List.<T>of() );
    }

    public <T> List<T> getList( String key, Class<T> clazz, List<T> defaultValue )
    {
        return (List<T>) this.getOrDefault( key, defaultValue );
    }

    public int size()
    {
        return this.map.size();
    }

    public boolean isEmpty()
    {
        return this.map.isEmpty();
    }

    public boolean containsValue( Object value )
    {
        return this.map.containsValue( value );
    }

    public boolean containsKey( Object key )
    {
        return this.map.containsKey( key );
    }

    public Object put( String key, Object value )
    {
        throw new UnsupportedOperationException( "Any is immutable." );
    }

    public void putAll( Map<? extends String, ?> map )
    {
        throw new UnsupportedOperationException( "Any is immutable." );
    }
    public void replaceAll( Map<? extends String, ?> map )
    {
        throw new UnsupportedOperationException( "Any is immutable." );
    }

    public Object remove( Object key )
    {
        throw new UnsupportedOperationException( "Any is immutable." );
    }

    public void clear()
    {
        throw new UnsupportedOperationException( "Any is immutable." );
    }

    public Set<String> keySet()
    {
        return this.map.keySet();
    }

    public Collection<Object> values()
    {
        return this.map.values();
    }

    public Set<Entry<String, Object>> entrySet()
    {
        return this.map.entrySet();
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        } else if ( o != null && this.getClass() == o.getClass() )
        {
            var document = (Any) o;
            return this.map.equals( document.map );
        } else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return this.map.hashCode();
    }

    public String toString()
    {
        return "{" + this.map + '}';
    }

    public static Builder bldr()
    {
        return new Builder();
    }

    public static Builder bldr( int size )
    {
        return new Builder( size );
    }


    public static class Builder
    {
        private Map<String, Object> map;

        private static Map<String, Function<Object, Object>> converters = new HashMap<>();
        private static Set<Class<?>> whitelist = Set.of( BigDecimal.class, OffsetDateTime.class, LocalDateTime.class, LocalDate.class, Boolean.class, String.class, Byte.class, Character.class, Short.class, Integer.class, Long.class, Double.class, Float.class, Void.class );

        private static Function<String, String> keyConverter = String::intern;

        public static void setKeyConverter( Function<String, String> c )
        {
            keyConverter = c;
        }

        public static Function<String, String> getKeyConverter()
        {
            return keyConverter;
        }

        public Builder()
        {
            this.map = new ConcurrentHashMap<>();
        }

        public Builder( int size )
        {
            this.map = new ConcurrentHashMap<>( size );
        }

        public static void setConverters( Map<String, Function<Object, Object>> c )
        {
            converters = Collections.unmodifiableMap( c );
        }

        public static Map<String, Function<Object, Object>> getConverters()
        {
            return converters;
        }

        public static void setWhitelist( Set<Class<?>> types )
        {
            whitelist = Collections.unmodifiableSet( types );
        }

        public static Set<Class<?>> getWhitelist()
        {
            return whitelist;
        }

        public Builder rename( String key, String newKey )
        {
            if ( key == newKey ) return this;
            Object obj = map.remove( key );
            map.put( keyConverter.apply( newKey ), obj );
            return this;
        }

        public Builder with( String key, Object value )
        {
            if ( value == null ) return this;
            map.put( keyConverter.apply( key ), clone( value ) );
            return this;
        }

        public Builder with( String key, Supplier<?> supplier )
        {
            final var value = supplier.get();
            if ( value == null ) return this;
            map.put( keyConverter.apply( key ), clone( value ) );
            return this;
        }

        public Builder without( String key )
        {
            map.remove( key );
            return this;
        }

        private Any cloneMap( Map<String, Object> theMap )
        {
            var bldr = Any.bldr( theMap.size() );

            for ( Map.Entry<String, Object> entry : theMap.entrySet() )
                bldr = bldr.with( keyConverter.apply( entry.getKey() ), clone( entry.getValue() ) );

            return bldr.build();
        }


        private List cloneList( List c, Supplier<List> factory )
        {
            final var clone = factory.get();

            for ( Object entry : c )
                clone.add( clone( entry ) );

            return Collections.unmodifiableList( clone );
        }

        private Set cloneSet( Set c, Supplier<Set> factory )
        {
            final var clone = factory.get();

            for ( Object entry : c )
                clone.add( clone( entry ) );
            return Collections.unmodifiableSet( clone );
        }

        Object clone( Object value )
        {
            if ( value == null )
                return null;

            final var convertedValue =
            converters.getOrDefault( value.getClass().getSimpleName(), i -> i )
                      .apply( value );

            if ( convertedValue == null )
                return null;

            final var convertedClazz = convertedValue.getClass();

            if ( isPrimitive.test( convertedClazz ) )
                return convertedValue;
            else if ( Map.class.isAssignableFrom( convertedClazz ) )
                return cloneMap( (Map) convertedValue );
            else if ( List.class.isAssignableFrom( convertedClazz ) )
                return cloneList( (List) convertedValue, () -> new ArrayList( ( (List) convertedValue ).size() ) );
            else if ( Set.class.isAssignableFrom( convertedClazz ) )
                return cloneSet( (Set) convertedValue, () -> new HashSet( ( (Set) convertedValue ).size() ) );
            else if ( convertedClazz.isArray() && isPrimitive.test( convertedClazz.getComponentType() ) )
                return convertedValue;
            else
                throw new RuntimeException( "Unsupported type " + convertedClazz.getSimpleName() );
        }

        public Builder as( Map<String, ? extends Object> m )
        {
            final var clone = (Map) clone( m );
            if ( clone != null )
                this.map.putAll( clone );
            return this;
        }

        Predicate<Class> isPrimitive = clazz -> clazz == Any.class |
        clazz.isPrimitive() |
        whitelist.contains( clazz ) |
        clazz.isRecord() |
        clazz.isEnum();

        public Any build()
        {
            return new Any( map );
        }
    }
}
