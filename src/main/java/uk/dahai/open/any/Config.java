package uk.dahai.open.any;

import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

import java.util.Map;
import java.util.function.Supplier;

import static com.fasterxml.jackson.databind.DeserializationFeature.USE_LONG_FOR_INTS;
import static com.fasterxml.jackson.databind.SerializationFeature.FAIL_ON_EMPTY_BEANS;

public class Config
{
    public static Supplier<ObjectMapper> mapper = () ->
    {
        final var objectMapper = new ObjectMapper();
        objectMapper.configure( FAIL_ON_EMPTY_BEANS, false );
        objectMapper.configure( USE_LONG_FOR_INTS, true );
        final var module = new SimpleModule( "any", Version.unknownVersion() );
        module.addAbstractTypeMapping( Map.class, Any.class );
        objectMapper.registerModule( module );
        return objectMapper;
    };

}
