package uk.dahai.open.any;

import org.junit.jupiter.api.Test;

import java.util.function.BiPredicate;

import static org.junit.jupiter.api.Assertions.*;

public class AnyTest
{
    @Test
    public void testOf()
    {
        final var vo = Any.of( "key", "value" );

        assertEquals( 1, vo.size() );
        assertEquals( "value", vo.getString( "key" ) );
    }

    @Test
    public void testOfBi()
    {
        final var vo = Any.of( "key-1", "value-1", "key-2", "value-2" );

        assertEquals( 2, vo.size() );
        assertEquals( "value-1", vo.getString( "key-1" ) );
        assertEquals( "value-2", vo.getString( "key-2" ) );
    }


    @Test
    public void testGet()
    {
        final var vo = Any.of( "key-1", "value-1", "key-2", 2 );

        assertEquals( "value-1", vo.get( "key-1" ) );
        assertEquals( 2, vo.get( "key-2" ) );
        assertThrows( ClassCastException.class, () -> vo.get( "key-2", Long.class ) );
        assertNull( vo.get( "key-4" ) );

    }

    @Test
    public void testGetClazz()
    {
        final var vo = Any.of( "key-1", "value-1", "key-2", 2 );

        assertEquals( "value-1", vo.get( "key-1", String.class ) );
        assertEquals( 2, vo.get( "key-2", Integer.class ) );
        assertThrows( ClassCastException.class, () -> vo.get( "key-2", Long.class ) );
        assertNull( vo.get( "key-4", String.class ) );

    }

    @Test
    public void testGetWithDefault()
    {
        final var vo = Any.of( "key-1", "value-1", "key-3", 3 );

        assertEquals( "value-1", vo.get( "key-1", "bosh" ) );
        assertEquals( "bosh", vo.get( "key-2", "bosh" ) );
        assertEquals( 3, vo.get( "key-3", "bosh" ) );
    }

    @Test
    public void testGetInteger()
    {
        final var vo = Any.of( "key-1", 1, "key-2", "hi" );

        assertEquals( 1, vo.getInteger( "key-1" ) );
        assertThrows( ClassCastException.class, () -> vo.getInteger( "key-2" ) );
        assertNull( vo.getInteger( "key-3" ) );
    }

    @Test
    public void testGetLong()
    {
        final var vo = Any.of( "key-1", 1l, "key-2", "hi" );

        assertEquals( 1, vo.getLong( "key-1" ) );
        assertThrows( ClassCastException.class, () -> vo.getLong( "key-2" ) );
        assertNull( vo.getLong( "key-3" ) );
    }

    @Test
    public void testGetDouble()
    {
        final var vo = Any.of( "key-1", 1.0d, "key-2", "hi" );

        assertEquals( 1.0d, vo.getDouble( "key-1" ) );
        assertThrows( ClassCastException.class, () -> vo.getDouble( "key-2" ) );
        assertNull( vo.getDouble( "key-3" ) );
    }

    @Test
    public void testGetBoolean()
    {
        final var vo = Any.of( "key-1", true, "key-2", "hi" );

        assertEquals( true, vo.getBoolean( "key-1" ) );
        assertThrows( ClassCastException.class, () -> vo.getBoolean( "key-2" ) );
        assertNull( vo.getBoolean( "key-3" ) );
    }

}
